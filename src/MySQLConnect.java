
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author himan
 */
public class MySQLConnect {
    /*
     * The below method creates a connection with mysql database named as 'student_mgmt' with the user account named 'studylink' whose password is 'studylink'.
     * It returns the java.sql.Connection object if the connection was successfully established otherwise returns 'null'
     */
    public static Connection getConnection() {
        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_mgmt", "root", "");
            return conn;
        }catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Connection Failed " + e.getMessage());
            return null;
        }
    }
    
//    public static void main(String args[]) {
//        Connection conn = getConnection();
//        if(conn!=null) {
//            JOptionPane.showMessageDialog(null, "Connection Established!");
//        }
//    }
}
